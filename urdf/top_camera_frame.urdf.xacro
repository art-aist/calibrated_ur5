<?xml version="1.0" ?>
<robot xmlns:xacro="http://wiki.ros.org/xacro" name="top_camera_frame"> 


  <!-- basic parts of the frame -->
  <xacro:include filename="$(find calibrated_ur5
			   )/urdf/mybox.urdf.xacro"/>

  <xacro:include filename="$(find calibrated_ur5
        )/urdf/mybeam.urdf.xacro"/>
  
  <xacro:include filename="$(find calibrated_ur5
          )/urdf/top_camera.urdf.xacro"/>

  <!-- parameters of the beasm -->
  <xacro:property name="beam_width"	value="0.040" />
  <xacro:property name="beam_height"	value="0.040" />


  <xacro:property name="beam_collision_margin" value="0.08" />

  <xacro:property name="long_beam_length"	value="1.928" />
  <xacro:property name="short_beam_length"	value="0.695" />

  <!-- geometric parameters for usb camera -->
  <xacro:property name="camera_height" value="0.02" />
  <xacro:property name="camera_rad" value="0.01" />
  <xacro:property name="camera_displacement_x" value="0.02" />




  <!-- long beam -->
  <xacro:macro name="long_beam" params="prefix parent *origin">
    <link name="${prefix}_long_beam_origin"/>
    <xacro:mybeam name="${prefix}long_beam_body"
      width="${beam_width}"
      length="${long_beam_length}"
      collision_margin="${beam_collision_margin}"
      parent="${parent}">
      <xacro:insert_block name="origin" />
    </xacro:mybeam>

    <joint name="${prefix}long_beam_joint" type="fixed">
      <parent link="${parent}"/>
      <child  link="${prefix}_long_beam_origin"/>
      <xacro:insert_block name="origin"/>
    </joint>
  </xacro:macro>


  <!-- short beam -->
  <xacro:macro name="short_beam" params="prefix parent *origin">
    <link name="${prefix}_short_beam_origin"/>
    <xacro:mybeam name="${prefix}short_beam_body"
      width="${beam_width}"
      length="${short_beam_length}"
      collision_margin="${beam_collision_margin}"
      parent="${parent}">
      <xacro:insert_block name="origin" />
    </xacro:mybeam>

    <joint name="${prefix}short_beam_joint" type="fixed">
      <parent link="${parent}"/>
      <child  link="${prefix}_short_beam_origin"/>
      <xacro:insert_block name="origin"/>
    </joint>
  </xacro:macro>

  
  <xacro:property name="bottom_front_beam_offset_x" value="-1.1" />
  <xacro:property name="bottom_front_beam_offset_y" value="-0.4" />
  <xacro:property name="bottom_front_beam_rot" value="${0.5 * pi}" />

  <xacro:property name="bottom_rear_beam_offset_x" value="${bottom_front_beam_offset_x}" />
  <xacro:property name="bottom_rear_beam_offset_y" value="${bottom_front_beam_offset_y + short_beam_length + beam_width}" />
  <xacro:property name="bottom_rear_beam_rot" value="${bottom_front_beam_rot}" />

  <xacro:property name="vertical_front_beam_offset_x" value="-0.08" />
  <xacro:property name="vertical_front_beam_offset_y" value="${bottom_front_beam_offset_y}" />
  <xacro:property name="vertical_front_beam_offset_z" value="0.002" />

  <xacro:property name="vertical_rear_beam_offset_x" value="${vertical_front_beam_offset_x}" />
  <xacro:property name="vertical_rear_beam_offset_y" value="${bottom_rear_beam_offset_y}" />
  <xacro:property name="vertical_rear_beam_offset_z" value="${vertical_front_beam_offset_z}" />

  <xacro:property name="short_beam_rot" value="${0.5 * pi}" />
  <xacro:property name="short_beam_offset_x" value="${vertical_front_beam_offset_x}" />
  <xacro:property name="short_beam_offset_y" value="${bottom_rear_beam_offset_y}" />


  <xacro:property name="short_beam_bottom_offset_z" value="${-beam_height}" />
  <xacro:property name="short_beam_top_offset_z" value="${-beam_height + 1.515}" />
  
  
  <xacro:macro name="top_camera_frame" params="prefix parent *origin">

    <link name="${prefix}top_camera_frame_origin"/>

    <joint name="${prefix}top_camera_frame_joint" type="fixed">
      <parent link="${parent}"/>
      <child  link="${prefix}top_camera_frame_origin"/>
      <xacro:insert_block name="origin"/>
    </joint>

  
    <!-- the horizontal beam near by UR5 -->
    <xacro:long_beam prefix="bottom_front_"
      parent="${prefix}top_camera_frame_origin">
      <origin
        xyz="${bottom_front_beam_offset_x} ${bottom_front_beam_offset_y} 0" 
        rpy="0 ${bottom_front_beam_rot} 0" 
      />
    </xacro:long_beam>

    
    <!-- the horizontal beam far from UR5 -->
    <xacro:long_beam prefix="bottom_rear_"
      parent="${prefix}top_camera_frame_origin">
      <origin
        xyz="${bottom_rear_beam_offset_x} ${bottom_rear_beam_offset_y} 0" 
        rpy="0 ${bottom_rear_beam_rot} 0" 
      />
    </xacro:long_beam>


    <!-- the vertical beam near UR5 -->
    <xacro:long_beam prefix="vertical_front_"
      parent="${prefix}top_camera_frame_origin">
      <origin
        xyz="${vertical_front_beam_offset_x} ${vertical_front_beam_offset_y} ${vertical_front_beam_offset_z}" 
        rpy="0 0 0" 
      />
    </xacro:long_beam>


    <!-- the vertical beam far from UR5 -->
    <xacro:long_beam prefix="vertical_rear_"
      parent="${prefix}top_camera_frame_origin">
      <origin
        xyz="${vertical_rear_beam_offset_x} ${vertical_rear_beam_offset_y} ${vertical_rear_beam_offset_z}" 
        rpy="0 0 0" 
      />
    </xacro:long_beam>


    <!-- the short beam : on the ground -->
    <xacro:short_beam prefix="short_beam_ground_"
      parent="${prefix}top_camera_frame_origin">
      <origin
        xyz="${short_beam_offset_x} ${short_beam_offset_y} ${short_beam_bottom_offset_z}" 
        rpy="${short_beam_rot} 0 0" 
      />
    </xacro:short_beam>

    
    <!-- the short beam : to this the usb camera is set -->
    <xacro:short_beam prefix="short_beam_top_"
      parent="${prefix}top_camera_frame_origin">
      <origin
        xyz="${short_beam_offset_x} ${short_beam_offset_y} ${short_beam_top_offset_z}" 
        rpy="${short_beam_rot} 0 0" 
      />
    </xacro:short_beam>


    <!-- the usb camera-->
    <link name="${prefix}usb_camera_origin"/>

    <joint name="${prefix}top_camera_joint" type="fixed">
      <parent link="${parent}"/>
      <child  link="${prefix}usb_camera_origin"/>
      <xacro:insert_block name="origin"/>
    </joint>


    <xacro:top_camera name="top_camera" 
      parent="${prefix}usb_camera_origin"
      camera_height="${camera_height}"
      camera_rad="${camera_rad}" >

      <origin
        xyz="${short_beam_offset_x - 2*camera_rad - camera_displacement_x} ${-camera_rad} ${short_beam_top_offset_z - camera_height}" 
        rpy="0 0 0" 
      />
    </xacro:top_camera>


  </xacro:macro>

</robot>
